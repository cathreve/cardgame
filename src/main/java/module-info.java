module com.example.kortspill {
  requires javafx.controls;
  requires javafx.fxml;


  opens com.example.kortspill to javafx.fxml;
  exports com.example.kortspill.model;
  opens com.example.kortspill.model to javafx.fxml;
  exports com.example.kortspill.controller;
  opens com.example.kortspill.controller to javafx.fxml;
  exports com.example.kortspill;
}