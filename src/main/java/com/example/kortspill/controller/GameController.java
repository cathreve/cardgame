package com.example.kortspill.controller;

import com.example.kortspill.model.DeckOfCards;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * Controller for dealing and displaying cards.
 *
 * @author Cathrine
 * @version 1.0
 * @since 14.03.24
 */
public class GameController {

  /**
   * Deals a specified number of cards and sorts them into
   * their respective containers based on their suits.
   *
   * @param numCards    The number of cards to deal.
   * @param containerH  The HBox container for hearts.
   * @param containerD  The HBox container for diamonds.
   * @param containerC  The HBox container for clubs.
   * @param containerS  The HBox container for spades.
   * @return            A list of strings representing the dealt cards.
   * @throws FileNotFoundException If the image file for the card cannot be found.
   */
  public List<String> dealCards(
          int numCards, HBox containerH, HBox containerD, HBox containerC, HBox containerS)
          throws FileNotFoundException {
    containerH.getChildren().clear();
    containerD.getChildren().clear();
    containerC.getChildren().clear();
    containerS.getChildren().clear();

    DeckOfCards deck = new DeckOfCards();
    List<String> dealtCards = deck.dealHand(numCards);
    for (String card : dealtCards) {
      char suit = card.charAt(0);
      switch (suit) {
        case 'C' -> containerC.getChildren().add(createCard(card));
        case 'D' -> containerD.getChildren().add(createCard(card));
        case 'H' -> containerH.getChildren().add(createCard(card));
        case 'S' -> containerS.getChildren().add(createCard(card));
        default -> {
        }
      }
    }
    return dealtCards;
  }

  /**
   * Creates an ImageView for a specified card name, loading the corresponding image file.
   *
   * @param cardName The name of the card (e.g., "H10" for the 10 of hearts).
   * @return         An ImageView object representing the card's image.
   */
  public ImageView createCard(String cardName) {
    String imagePath = "/playingCards/" + cardName + ".png";
    Image cardImg = new Image(Objects.requireNonNull(getClass().getResourceAsStream(imagePath)));

    ImageView imageView = new ImageView(cardImg);
    imageView.setFitWidth(62.5);
    imageView.setFitHeight(90.75);

    return imageView;
  }

  /**
   * Displays a result message in the specified result box.
   *
   * @param resultBox The HBox where the result message will be displayed.
   * @param result    The result message to be displayed.
   */
  public void displayResult(HBox resultBox, String result) {
    resultBox.getChildren().add(new Button(result));
  }
}
