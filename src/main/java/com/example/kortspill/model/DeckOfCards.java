package com.example.kortspill.model;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Represents a deck of 52 cards.
 *
 * @author Cathrine
 * @version 3.0
 * @since 11.03.24
 */
public class DeckOfCards {
  private final String[] suits
          = { "C", "D", "H", "S" }; // 'C'=clubs, 'D'=diamonds, 'H'=heart, 'S'=spade
  private final Random random;

  public DeckOfCards() {
    this.random = new Random();
  }

  /**
   * //Help from chat.//
   * Generates a deck of 52 cards that match the names of
   * picture-file names in the resources file.
   *
   * @return a list representing the deck of cards
   */
  public Stream<String> generateDeck() {
    return Stream.of(suits)
            .flatMap(suit -> IntStream.range(1, 14)
                    .mapToObj(cardValue -> suit + cardValue));
  }

  /**
   * Shuffles the entire deck of cards.
   *
   * @return a list representing the shuffled deck
   */
  public List<String> shuffledCards() {
    List<String> deck = generateDeck().collect(Collectors.toList());
    Collections.shuffle(deck, random);
    return deck;
  }

  /**
   * Deals a hand of cards.
   *
   * @param n the number of cards in the hand
   * @return a list representing the hand
   */
  public List<String> dealHand(int n) {
    if (n < 5) {
      throw new IllegalArgumentException("Can't deal less than 5 cards");
    }
    if (n > shuffledCards().size()) {
      throw new IllegalArgumentException("There are only " + shuffledCards().size()
              + " cards in the deck");
    }
    return shuffledCards().subList(0, n);
  }
}
