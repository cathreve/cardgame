package com.example.kortspill.model;


import java.util.List;
import java.util.Objects;

/**
 * Class to analyse the dealt hand of cards.
 *
 * @author Cathrine
 * @version 1.0
 * @since 13.03.24
 */
public class CheckHand {
  private final List<String> dealtCards;


  public CheckHand(List<String> dealtCards) {
    this.dealtCards = dealtCards;
  }

  /**
   * Calculates the sum of the values of the cards in the hand.
   * card.length() - 4 removes ".png" from the name
   *
   * @return the sum of the values of the cards
   */
  public int cardsSum() {
    int sum = 0;
    for (String card : dealtCards) {
      String valueString = card.substring(1);
      int cardValue = Integer.parseInt(valueString);
      sum += cardValue;
    }
    return sum;
  }

  /**
   * String of only dealt cards of suit hearts.
   * TODO   * Dersom det ikke er noen Hjerter på hånd,
   *        * kan tekstfeltet inneholde teksten "No Hearts", for eksempel.
   *
   * @return string of card names in form "H12 H9 H1".
   */
  public StringBuilder onlyHearts() {
    StringBuilder hearts = new StringBuilder();
    for (String card : dealtCards) {
      if (card.charAt(0) == 'H') {
        hearts.append(card).append(" ");
      }
    }
    return hearts;
  }

  /**
   * Checks if spare queen is in the dealt cards.
   *
   * @return boolean, true if S12 is in dealt cards, false of not
   */
  public boolean spareQueen() {
    boolean exists = false;
    for (String card : dealtCards) {
      if (Objects.equals(card, "S12")) {
        exists = true;
        break;
      }
    }
    return exists;
  }

  /**
   * Checks if dealt hand is a 5-flush or not.
   *
   * @return true if a 5-flush exists, false if not
   */
  public boolean flush() {
    boolean exists = false;
    int c = 0;
    int d = 0;
    int h = 0;
    int s = 0;

    for (String card : dealtCards) {
      char suit = card.charAt(0);
      switch (suit) {
        case 'C' -> c += 1;
        case 'D' -> d += 1;
        case 'H' -> h += 1;
        case 'S' -> s += 1;
        default -> { }
      }
      if (c == 5 || d == 5 || h == 5 || s == 5) {
        exists = true;
        break;
      }
    }
    return exists;
  }
}
