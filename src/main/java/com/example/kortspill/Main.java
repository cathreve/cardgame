package com.example.kortspill;

import com.example.kortspill.controller.GameController;
import com.example.kortspill.model.CheckHand;
import java.io.FileNotFoundException;
import java.util.List;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Main view of the application.
 *
 * @author Cathrine
 * @version 1.0
 * @since 12.03.24
 */
public class Main extends Application {
  private final GameController gameController = new GameController();

  public static void main(String[] args) {
    launch();
  }

  @Override
  public void start(Stage primaryStage) {
    Spinner<Integer> spinnerDealCards = new Spinner<>();
    spinnerDealCards.setValueFactory(
            new SpinnerValueFactory.IntegerSpinnerValueFactory(5, 52, 5));

    Button confirmButton = new Button("Deal Cards");
    Button checkHandButton = new Button("Check hand");

    HBox spinnerBox = new HBox(spinnerDealCards, confirmButton, checkHandButton);
    spinnerBox.setSpacing(10);
    spinnerBox.setPadding(new Insets(10));

    HBox resultBox = new HBox();
    resultBox.setSpacing(10);
    resultBox.setPadding(new Insets(10));

    HBox containerH = new HBox();
    HBox containerD = new HBox();
    HBox containerC = new HBox();
    HBox containerS = new HBox();

    VBox cardContainer = new VBox();
    cardContainer.getChildren().addAll(
            spinnerBox, resultBox, containerC, containerD, containerH, containerS);

    BorderPane borderRoot = new BorderPane();
    borderRoot.setCenter(cardContainer);
    Scene scene = new Scene(borderRoot, 900, 700);
    primaryStage.setScene(scene);
    primaryStage.show();

    // Deal cards once and reuse the dealt cards for both displaying cards and hand evaluation
    confirmButton.setOnAction(event -> {
      int numCardsToDeal = spinnerDealCards.getValue();
      try {
        List<String> dealtCards = gameController.dealCards(
                numCardsToDeal, containerC, containerD, containerH, containerS);
        checkHandButton.setOnAction(checkEvent -> {
          CheckHand checkHand = new CheckHand(dealtCards);

          // Clear the result box before displaying new results
          resultBox.getChildren().clear();

          int sum = checkHand.cardsSum();
          gameController.displayResult(resultBox, "Total sum of card values: " + sum);

          StringBuilder hearts = checkHand.onlyHearts();
          if (hearts.isEmpty()) {
            gameController.displayResult(resultBox, "No Hearts");
          } else {
            gameController.displayResult(resultBox, hearts.toString());
          }

          boolean hasSpareQueen = checkHand.spareQueen();
          gameController.displayResult(resultBox, "Spare Queen in hand: "
                  + (hasSpareQueen ? "Yes" : "No"));

          boolean isFlush = checkHand.flush();
          gameController.displayResult(resultBox, "5-flush exists: " + (isFlush ? "Yes" : "No"));
        });
      } catch (FileNotFoundException e) {
        throw new RuntimeException(e);
      }
    });
  }
}
