package com.example.kortspill;

import com.example.kortspill.model.DeckOfCards;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;


import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {


  @Nested
  @DisplayName("Positive test for Julia transformation")
  public class PositiveDeckOfCardsTest{
    @Test
    @DisplayName("Right amount of cards in deck")
    public void testGenerateDeck() {
      DeckOfCards deck = new DeckOfCards();
      List<String> generated = deck.generateDeck().collect(Collectors.toList());
      assertNotNull(generated);
      assertEquals(52, generated.size());
    }
    @Test
    @DisplayName("Right amount of cards shuffled")
    public void testShuffledCards() {
      DeckOfCards deck = new DeckOfCards();
      List<String> shuffled = deck.shuffledCards();
      assertNotNull(shuffled);
      assertEquals(52, shuffled.size());
    }
    @Test
    @DisplayName("Right amount of cards in hand")
    public void testDealHand5Cards() {
      DeckOfCards deck = new DeckOfCards();
      List<String> hand = deck.dealHand(5);
      assertNotNull(hand);
      assertEquals(5, hand.size());
    }
    @Test
    @DisplayName("Right amount of cards in hand")
    public void testDealHand52Cards() {
      DeckOfCards deck = new DeckOfCards();
      List<String> hand = deck.dealHand(52);
      assertNotNull(hand);
      assertEquals(52, hand.size());
    }
    @Test
    @DisplayName("Does not throw on 5")
    public void doesNotThrowOn5() {
      DeckOfCards deck = new DeckOfCards();
      assertDoesNotThrow(()->
      {
        List<String> hand = deck.dealHand(5);
      }
      );
    }

    @Test
    @DisplayName("Does not throw on 52")
    public void doesNotThrowOn52() {
      DeckOfCards deck = new DeckOfCards();
      assertDoesNotThrow(()->
              {
                List<String> hand = deck.dealHand(52);
              }
      );
    }
  }

  @Nested
  @DisplayName("Negative test for Julia transformation")
  public class NegativeDeckOfCardsTest{
    @Test
    @DisplayName("Throws for too few cards")
    public void test4Cards() {
      DeckOfCards deck = new DeckOfCards();
      assertThrows(IllegalArgumentException.class, () -> {
        List<String> hand = deck.dealHand(4);
      });
    }
    @Test
    @DisplayName("Throws for too many cards")
    public void test53Cards() {
      DeckOfCards deck = new DeckOfCards();
      assertThrows(IllegalArgumentException.class, () -> {
        List<String> hand = deck.dealHand(53);
      });
    }
  }
}